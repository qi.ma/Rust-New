# rust-new

An interactive CLI tool to create new Rust projects.

Example dialogue:

```text
Creating new Rust project
=========================
Current settings:
* project type:           binary (application)
* project's name:         tester
* project's path:         Tester
* project's description:  A simple test project.
Are these values correct? [Y/n]:
A folder already exists at "Tester".  Should it be removed? [y/N]: y
Successfully created tester.
```

## License

`rust-new` is distributed under the terms of the General Public License (GPL), version 3
([COPYING](COPYING) or http://www.gnu.org/licenses/gpl-3.0.en.html).
