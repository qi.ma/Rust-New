use super::input_getter::{get_bool, get_string};
use file_contents;
use std::ffi::OsStr;
use std::fs::{self, Metadata};
use std::io::{self, Stdin};
use std::path::PathBuf;
use std::process::Command;
use std::thread::sleep;
use std::time::Duration;

/// The kind of project to be created.
pub enum Kind {
    /// A binary (application).
    Binary,
    /// A library.
    Library,
}

/// The main struct, holding the settings for the new project.
#[cfg_attr(feature = "cargo-clippy", allow(missing_docs_in_private_items))]
pub struct Project {
    kind: Kind,
    name: String,
    path: PathBuf,
    description: String,
    stdin: Stdin,
}

impl Default for Project {
    fn default() -> Project {
        prnt_ln!("\nCreating new Rust project");
        prnt_ln!("=========================");
        Project {
            kind: Kind::Binary,
            name: "tester".to_string(),
            path: PathBuf::from(OsStr::new("Tester")),
            description: "A simple test project.".to_string(),
            stdin: io::stdin(),
        }
    }
}

#[cfg_attr(feature = "cargo-clippy", allow(use_debug))]
impl Project {
    /// Prints the project's current settings.
    pub fn display(&mut self) {
        prnt_ln!("Current settings:");
        prnt!("* project type:           ");
        match self.kind {
            Kind::Binary => dark_cyan_ln!("binary (application)"),
            Kind::Library => dark_cyan_ln!("library"),
        }
        prnt!("* project's name:         ");
        dark_cyan_ln!("{}", self.name);
        prnt!("* project's path:         ");
        dark_cyan_ln!("{}", self.path.to_string_lossy());
        prnt!("* project's description:  ");
        dark_cyan_ln!("{}", self.description);
    }

    /// Asks the user for confirmation that the project's current settings are suitable.
    pub fn approve_settings(&mut self) -> bool {
        self.display();
        loop {
            yellow!("Are these values correct? [Y/n]: ");
            match get_bool(&mut self.stdin.lock(), Some(true)) {
                Err(error) => {
                    red_ln!("{}", error);
                }
                Ok(false) => return false,
                Ok(true) => return true,
            }
        }
    }

    /// Gets the user to enter new values for the project's settings.
    pub fn get_settings(&mut self) {
        self.get_kind();
        self.get_name();
        self.get_path();
        self.get_description();
        prnt_ln!("");
    }

    /// Checks the root project path can be created, or if a file/folder already exists there, asks
    /// the user if it should be deleted and removes it if so.
    pub fn check_and_clear_path(&self) -> bool {
        let is_dir: bool = match fs::create_dir(&self.path) {
            Ok(()) => return true,
            Err(error) => {
                if let io::ErrorKind::AlreadyExists = error.kind() {
                    unwrap!(fs::metadata(&self.path).as_ref().map(Metadata::is_dir))
                } else {
                    red_ln!("{:?} is an invalid path: {}", self.path, error);
                    return false;
                }
            }
        };

        // File/folder exists as `self.path` - seek permission to remove it.
        loop {
            if is_dir {
                yellow!("A folder");
            } else {
                yellow!("A file");
            }
            yellow!(" already exists at {:?}.  Should it be removed? [y/N]: ", self.path);
            match get_bool(&mut self.stdin.lock(), Some(false)) {
                Err(error) => {
                    red_ln!("{}", error);
                }
                Ok(false) => return false,
                Ok(true) => break,
            }
        }

        // Try to remove the file/folder at `root.path`.
        if let Err(error) = if is_dir {
               fs::remove_dir_all(&self.path)
           } else {
               fs::remove_file(&self.path)
           } {
            red_ln!("Failed to remove {:?}: {}", self.path, error);
            return false;
        }

        sleep(Duration::from_millis(250));

        match fs::create_dir(&self.path) {
            Ok(()) => true,
            Err(error) => {
                red_ln!("Removed {:?} but unable to create new folder there: {}", self.path, error);
                false
            }
        }
    }

    /// Creates all the files and folders of the new project.
    pub fn create(&mut self) {
        file_contents::create_gitignore(self.path.clone());
        file_contents::create_cargo_toml(self.path.clone(), &self.name, &self.description);
        file_contents::create_license_mit(self.path.clone());
        file_contents::create_license_apache(self.path.clone());
        file_contents::create_sublime_project(self.path.clone(), self.name_to_path());
        file_contents::create_readme(self.path.clone(), &self.name, &self.description);
        file_contents::create_rustfmt_toml(self.path.clone());
        match self.kind {
            Kind::Binary => file_contents::create_main_rs(self.path.clone(), &self.description),
            Kind::Library => file_contents::create_lib_rs(self.path.clone(), &self.description),
        }
        self.create_msvc_files();
        green_ln!("Successfully created {}.", self.name);
    }

    /// Opens the new project in Sublime Text 3 if available.
    pub fn open(&mut self) {
        let sublime_path = match get_sublime_path() {
            Some(path) => path,
            None => return,
        };

        let mut sublime_project_path = self.path.clone();
        sublime_project_path.push(self.name_to_path());
        assert!(sublime_project_path.set_extension("sublime-project"));

        let mut cargo_toml_path = self.path.clone();
        cargo_toml_path.push("Cargo.toml");

        let mut source_path = self.path.clone();
        source_path.push("src");
        match self.kind {
            Kind::Binary => source_path.push("main.rs"),
            Kind::Library => source_path.push("lib.rs"),
        }

        let _ = Command::new(sublime_path)
            .args(&["-n",
                    &sublime_project_path.to_string_lossy(),
                    &cargo_toml_path.to_string_lossy(),
                    &source_path.to_string_lossy()])
            .spawn();
    }

    /// Gets the user to choose which kind of project is to be created; a binary or library.
    fn get_kind(&mut self) {
        loop {
            prnt!("Create binary (application) project? (if 'n', a library project) [Y/n]: ");
            match get_bool(&mut self.stdin.lock(), Some(true)) {
                Err(error) => {
                    red_ln!("{}", error);
                }
                Ok(true) => {
                    self.kind = Kind::Binary;
                    return;
                }
                Ok(false) => {
                    self.kind = Kind::Library;
                    return;
                }
            }
        }
    }

    /// Gets the user to enter a name for the new project.  Validates that the name is lowercase
    /// with only single underscores.
    fn get_name(&mut self) {
        loop {
            prnt!("Enter project's name [{}]: ", self.name);
            match get_string(&mut self.stdin.lock()) {
                Err(error) => {
                    red_ln!("{}", error);
                }
                Ok(name) => {
                    if name.is_empty() {
                        return;
                    }
                    if name_is_valid(&name) {
                        self.name = name;
                        break;
                    } else {
                        red_ln!("Name must be lowercase with single underscores.");
                    }
                }
            }
        }
        self.path = self.name_to_path();
    }

    /// Gets the user to enter a path for the new project.
    pub fn get_path(&mut self) {
        loop {
            prnt!("Enter project's path [{}]: ", self.path.to_string_lossy());
            match get_string(&mut self.stdin.lock()) {
                Err(error) => {
                    red_ln!("{}", error);
                }
                Ok(path_as_string) => {
                    if !path_as_string.is_empty() {
                        self.path = PathBuf::from(OsStr::new(&path_as_string));
                    }
                    return;
                }
            }
        }
    }

    /// Gets the user to enter a description for the new project.
    fn get_description(&mut self) {
        loop {
            prnt!("Enter project's description [{}]: ", self.description);
            match get_string(&mut self.stdin.lock()) {
                Err(error) => {
                    red_ln!("{}", error);
                }
                Ok(description) => {
                    if !description.is_empty() {
                        self.description = description;
                    }
                    return;
                }
            }
        }
    }

    /// Converts a camel-case name to a properly-capitalised title.
    fn name_to_path(&self) -> PathBuf {
        let mut new_path_as_string = String::new();
        let mut next_is_uppercase = true;
        for c in self.name.chars() {
            if c == '_' {
                next_is_uppercase = true;
                new_path_as_string.push(' ');
            } else if next_is_uppercase {
                new_path_as_string += &c.to_uppercase().collect::<String>();
                next_is_uppercase = false;
            } else {
                new_path_as_string.push(c);
                next_is_uppercase = false;
            }
        }
        PathBuf::from(OsStr::new(&new_path_as_string))
    }

    /// Copies the natvis files from the rust sources if they can be found.  No-op for non-Windows.
    fn create_msvc_files(&self) {
        if cfg!(not(windows)) {
            return;
        }
        let mut msvc_path = self.path.clone();
        msvc_path.push("MSVC");
        unwrap!(fs::create_dir(&msvc_path));
        let is_binary = match self.kind {
            Kind::Binary => true,
            Kind::Library => false,
        };
        file_contents::create_msvc_solution(msvc_path.clone(),
                                            self.name_to_path(),
                                            &self.name,
                                            is_binary);
        file_contents::create_core_natvis(msvc_path.clone());
        file_contents::create_collections_natvis(msvc_path.clone());
        file_contents::create_alloc_natvis(msvc_path);
    }
}

/// Validates that the name is all lowercase interspersed with optional, single underscores.
fn name_is_valid(name: &str) -> bool {
    let mut previous_was_underscore = false;
    for c in name.chars() {
        if c == '_' {
            if previous_was_underscore {
                return false;
            } else {
                previous_was_underscore = true;
            }
        } else {
            if !c.is_lowercase() {
                return false;
            }
            previous_was_underscore = false;
        }
    }
    true
}

#[cfg(windows)]
/// Returns the path to Sublime Text 3 by reading it from the Windows registry.
fn get_sublime_path() -> Option<String> {
    use winreg::RegKey;
    use winreg::enums::{HKEY_CLASSES_ROOT, KEY_READ};

    let hkcr = RegKey::predef(HKEY_CLASSES_ROOT);
    if let Ok(subkey) = hkcr.open_subkey_with_flags("*\\shell\\Open with Sublime Text\\command",
                                                    KEY_READ) {
        let default: String = unwrap!(subkey.get_value(""));
        Some(default.rsplitn(2, ' ').collect::<Vec<_>>()[1].to_string())
    } else {
        None
    }
}

#[cfg(not(windows))]
/// Returns the path to Sublime Text 3 by running `which subl`.
fn get_sublime_path() -> Option<String> {
    Command::new("which")
        .arg("subl")
        .output()
        .ok()
        .and_then(|output| if output.status.success() {
                      Some(String::from_utf8_lossy(&output.stdout).trim().to_string())
                  } else {
                      None
                  })
}
