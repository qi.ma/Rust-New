//! An interactive CLI tool to create new Rust projects.

#![forbid(warnings)]
#![warn(missing_copy_implementations, trivial_casts, trivial_numeric_casts, unsafe_code,
        unused_extern_crates, unused_import_braces, unused_qualifications, unused_results,
        variant_size_differences)]
#![cfg_attr(feature="cargo-clippy", deny(clippy, clippy_pedantic))]

#[macro_use]
extern crate colour;
extern crate time;
#[macro_use]
extern crate unwrap;
#[cfg(windows)]
extern crate winreg;

/// Module for creating project files.
mod file_contents;
/// Reads and validates input from a stream.
mod input_getter;
/// Main struct which holds the requested settings for the new project along with functions to
/// modify these and generate the new project's folders and files.
mod project;

use project::Project;

/// The main entry-point.
fn main() {
    let mut project = Project::default();
    while !project.approve_settings() {
        project.get_settings();
    }
    while !project.check_and_clear_path() {
        project.get_path();
    }
    project.create();
    project.open();
}
