#![cfg_attr(feature="cargo-clippy", allow(missing_docs_in_private_items))]

use std::fs::{self, File};
use std::io::Write;
use std::path::PathBuf;
use time;

pub fn create_gitignore(mut path: PathBuf) {
    path.push(".gitignore");
    let mut file = unwrap!(File::create(&path));
    unwrap!(write!(
        &mut file,
        r#"Cargo.lock
target
*.sublime-workspace
*.sln
*/.vs
"#
    ));
}

pub fn create_cargo_toml(mut path: PathBuf, name: &str, description: &str) {
    path.push("Cargo.toml");
    let mut file = unwrap!(File::create(&path));
    unwrap!(write!(
        &mut file,
        r#"[package]
authors = ["Fraser Hutchison <fraser.hutchison@maidsafe.net>"]
description = "{1}"
license = "MIT/Apache-2.0"
name = "{0}"
readme = "README.md"
version = "0.5.0"

[dependencies]
unwrap = "1.1"
"#,
        name,
        description
    ));
}

pub fn create_sublime_project(mut path: PathBuf, filename: PathBuf) {
    path.push(filename);
    assert!(path.set_extension("sublime-project"));
    let mut file = unwrap!(File::create(&path));
    unwrap!(write!(
        &mut file,
        r#"{{
    "folders":
    [
        {{
            "path": "."
        }}
    ]
}}
"#
    ));
}

pub fn create_readme(mut path: PathBuf, name: &str, description: &str) {
    path.push("README.md");
    let mut file = unwrap!(File::create(&path));
    unwrap!(write!(
        &mut file,
        r#"# {0}

{1}

## License

`{0}` is licensed under either of

* MIT License ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)
* Apache License, Version 2.0 ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)

at your option.

## Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted for inclusion in the
work by you, as defined in the Apache-2.0 license, shall be dual licensed as above, without any
additional terms or conditions.
"#,
        name,
        description
    ));
}

pub fn create_rustfmt_toml(mut path: PathBuf) {
    path.push("rustfmt.toml");
    let mut file = unwrap!(File::create(&path));
    unwrap!(write!(
        &mut file,
        r#"array_layout = "Visual"
combine_control_expr = false
condense_wildcard_suffices = true
control_style = "Legacy"
fn_args_layout = "Visual"
fn_args_paren_newline = true
fn_call_style = "Visual"
fn_call_width = 100
fn_single_line = true
generics_indent = "Visual"
ideal_width = 100
newline_style = "Native"
reorder_imports = true
reorder_imported_names = true
single_line_if_else_max_width = 100
struct_lit_width = 100
struct_variant_width = 100
use_try_shorthand = true
where_style = "Legacy"
write_mode = "Overwrite"
"#
    ));
}

pub fn create_main_rs(mut path: PathBuf, description: &str) {
    path.push("src");
    unwrap!(fs::create_dir(&path));
    path.push("main.rs");
    let mut file = unwrap!(File::create(&path));
    unwrap!(write!(
        &mut file,
        r#"//! {}

#![forbid(warnings)]
#![warn(missing_copy_implementations, trivial_casts, trivial_numeric_casts, unsafe_code,
        unused_extern_crates, unused_import_braces, unused_qualifications, unused_results,
        variant_size_differences)]
#![cfg_attr(feature="cargo-clippy", deny(clippy, clippy_pedantic))]
#![cfg_attr(feature="cargo-clippy", allow(print_stdout, use_debug))]

// TODO - remove
#![allow(unused)]

#[macro_use]
extern crate unwrap;

fn main() {{
    println!("1879");
}}
"#,
        description
    ));
}

pub fn create_lib_rs(mut path: PathBuf, description: &str) {
    path.push("src");
    unwrap!(fs::create_dir(&path));
    path.push("lib.rs");
    let mut file = unwrap!(File::create(&path));
    unwrap!(write!(
        &mut file,
        r#"//! {}

#![forbid(warnings)]
#![warn(missing_copy_implementations, trivial_casts, trivial_numeric_casts, unsafe_code,
        unused_extern_crates, unused_import_braces, unused_qualifications, unused_results,
        variant_size_differences)]
#![cfg_attr(feature="cargo-clippy", deny(clippy, clippy_pedantic))]
#![cfg_attr(feature="cargo-clippy", allow(print_stdout, use_debug))]

// TODO - remove
#![allow(unused)]

#[macro_use]
extern crate unwrap;

#[cfg(test)]
mod tests {{
    #[test]
    fn scapa() {{
    }}
}}
"#,
        description
    ));
}

pub fn create_msvc_solution(mut path: PathBuf,
                            sln_filename: PathBuf,
                            cargo_name: &str,
                            is_binary: bool) {
    let mut starting_dir = PathBuf::new();
    for component in unwrap!(path.join("..").canonicalize()).iter() {
        starting_dir.push(unwrap!(component.to_str()).trim_left_matches(r"\\?\"));
    }
    starting_dir.push("target");
    starting_dir.push("stable");
    starting_dir.push("debug");
    let mut exe_path = starting_dir.join(cargo_name);
    assert!(exe_path.set_extension("exe"));
    let (exe_args, src_file) = if is_binary {
        ("", r"..\src\main.rs")
    } else {
        ("--nocapture --color=always", r"..\src\lib.rs")
    };

    path.push(sln_filename);
    assert!(path.set_extension("sln"));
    let mut file = unwrap!(File::create(&path));
    unwrap!(write!(
        &mut file,
        r#"Microsoft Visual Studio Solution File, Format Version 12.00
Project("{{911E67C6-3D85-4FCE-B560-20A9C3E3FF48}}") = "{0}", "{1}", "{{{5}}}"
{6}ProjectSection(DebuggerProjectSystem) = preProject
{6}{6}Executable = {1}
{6}{6}Arguments = {2}
{6}{6}StartingDirectory = {3}
{6}EndProjectSection
EndProject
Project("{{2150E333-8FDC-42A3-9474-1A3956D46DE8}}") = "Natvis", "Natvis", "{{{5}}}"
{6}ProjectSection(SolutionItems) = preProject
{6}{6}liballoc.natvis = liballoc.natvis
{6}{6}libcollections.natvis = libcollections.natvis
{6}{6}libcore.natvis = libcore.natvis
{6}EndProjectSection
EndProject
Project("{{2150E333-8FDC-42A3-9474-1A3956D46DE8}}") = "src", "src", "{{{5}}}"
{6}ProjectSection(SolutionItems) = preProject
{6}{6}{4} = {4}
{6}EndProjectSection
EndProject
"#,
        cargo_name,
        unwrap!(exe_path.to_str()),
        exe_args,
        unwrap!(starting_dir.to_str()),
        src_file,
        "00000000-0000-0000-0000-000000000000",
        '\u{9}'
    ));
}

#[cfg_attr(rustfmt, rustfmt_skip)]
pub fn create_core_natvis(mut path: PathBuf) {
    path.push("libcore.natvis");
    let mut file = unwrap!(File::create(&path));
    unwrap!(write!(&mut file,
                   r#"<?xml version="1.0" encoding="utf-8"?>
<AutoVisualizer xmlns="http://schemas.microsoft.com/vstudio/debugger/natvis/2010">
  <Type Name="core::ptr::Unique&lt;*&gt;">
    <DisplayString>{{{{ Unique {{*pointer.__0}} }}}}</DisplayString>
    <Expand>
      <Item Name="[ptr]">pointer.__0</Item>
    </Expand>
  </Type>
  <Type Name="core::ptr::Shared&lt;*&gt;">
    <DisplayString>{{{{ Shared {{*pointer.__0}} }}}}</DisplayString>
    <Expand>
      <Item Name="[ptr]">pointer.__0</Item>
    </Expand>
  </Type>
  <Type Name="core::option::Option&lt;*&gt;">
    <DisplayString Condition="RUST$ENUM$DISR == 0x0">{{{{ None }}}}</DisplayString>
    <DisplayString Condition="RUST$ENUM$DISR == 0x1">{{{{ Some {{__0}} }}}}</DisplayString>
    <Expand>
      <Item Name="[size]" ExcludeView="simple">(ULONG)(RUST$ENUM$DISR != 0)</Item>
      <Item Name="[value]" ExcludeView="simple">__0</Item>
      <ArrayItems>
        <Size>(ULONG)(RUST$ENUM$DISR != 0)</Size>
        <ValuePointer>&amp;__0</ValuePointer>
      </ArrayItems>
    </Expand>
  </Type>
  <Type Name="core::option::Option&lt;*&gt;" Priority="MediumLow">
    <DisplayString Condition="*(PVOID *)this == nullptr">{{{{ None }}}}</DisplayString>
    <DisplayString>{{{{ Some {{($T1 *)this}} }}}}</DisplayString>
    <Expand>
      <Item Name="[size]" ExcludeView="simple">(ULONG)(*(PVOID *)this != nullptr)</Item>
      <Item Name="[value]" ExcludeView="simple" Condition="*(PVOID *)this != nullptr">($T1 *)this</Item>
      <ArrayItems>
        <Size>(ULONG)(*(PVOID *)this != nullptr)</Size>
        <ValuePointer>($T1 *)this</ValuePointer>
      </ArrayItems>
    </Expand>
  </Type>
</AutoVisualizer>
"#));
}

#[cfg_attr(rustfmt, rustfmt_skip)]
pub fn create_collections_natvis(mut path: PathBuf) {
    path.push("libcollections.natvis");
    let mut file = unwrap!(File::create(&path));
    unwrap!(write!(&mut file,
                   r#"<?xml version="1.0" encoding="utf-8"?>
<AutoVisualizer xmlns="http://schemas.microsoft.com/vstudio/debugger/natvis/2010">
  <Type Name="collections::vec::Vec&lt;*&gt;">
    <DisplayString>{{{{ size={{len}} }}}}</DisplayString>
    <Expand>
      <Item Name="[size]" ExcludeView="simple">len</Item>
      <Item Name="[capacity]" ExcludeView="simple">buf.cap</Item>
      <ArrayItems>
        <Size>len</Size>
        <ValuePointer>buf.ptr.pointer.__0</ValuePointer>
      </ArrayItems>
    </Expand>
  </Type>
  <Type Name="collections::vec_deque::VecDeque&lt;*&gt;">
    <DisplayString>{{{{ size={{tail &lt;= head ? head - tail : buf.cap - tail + head}} }}}}</DisplayString>
    <Expand>
      <Item Name="[size]" ExcludeView="simple">tail &lt;= head ? head - tail : buf.cap - tail + head</Item>
      <Item Name="[capacity]" ExcludeView="simple">buf.cap</Item>
      <CustomListItems>
        <Variable Name="i" InitialValue="tail" />
        <Size>tail &lt;= head ? head - tail : buf.cap - tail + head</Size>
        <Loop>
          <If Condition="i == head">
            <Break/>
          </If>
          <Item>buf.ptr.pointer.__0 + i</Item>
          <Exec>i = (i + 1 == buf.cap ? 0 : i + 1)</Exec>
        </Loop>
      </CustomListItems>
    </Expand>
  </Type>
  <Type Name="collections::linked_list::LinkedList&lt;*&gt;">
    <DisplayString>{{{{ size={{len}} }}}}</DisplayString>
    <Expand>
      <LinkedListItems>
        <Size>len</Size>
        <HeadPointer>*(collections::linked_list::Node&lt;$T1&gt; **)&amp;head</HeadPointer>
        <NextPointer>*(collections::linked_list::Node&lt;$T1&gt; **)&amp;next</NextPointer>
        <ValueNode>element</ValueNode>
      </LinkedListItems>
    </Expand>
  </Type>
  <Type Name="collections::string::String">
    <DisplayString>{{*(char**)this,[vec.len]}}</DisplayString>
    <StringView>*(char**)this,[vec.len]</StringView>
    <Expand>
      <Item Name="[size]" ExcludeView="simple">vec.len</Item>
      <Item Name="[capacity]" ExcludeView="simple">vec.buf.cap</Item>
      <ArrayItems>
        <Size>vec.len</Size>
        <ValuePointer>*(char**)this</ValuePointer>
      </ArrayItems>
    </Expand>
  </Type>
</AutoVisualizer>
"#));
}

#[cfg_attr(rustfmt, rustfmt_skip)]
pub fn create_alloc_natvis(mut path: PathBuf) {
    path.push("liballoc.natvis");
    let mut file = unwrap!(File::create(&path));
    unwrap!(write!(&mut file,
                   r#"<?xml version="1.0" encoding="utf-8"?>
<AutoVisualizer xmlns="http://schemas.microsoft.com/vstudio/debugger/natvis/2010">
  <Type Name="alloc::vec::Vec&lt;*&gt;">
    <DisplayString>{{{{ size={{len}} }}}}</DisplayString>
    <Expand>
      <Item Name="[size]" ExcludeView="simple">len</Item>
      <Item Name="[capacity]" ExcludeView="simple">buf.cap</Item>
      <ArrayItems>
        <Size>len</Size>
        <ValuePointer>buf.ptr.pointer.__0</ValuePointer>
      </ArrayItems>
    </Expand>
  </Type>
  <Type Name="alloc::vec_deque::VecDeque&lt;*&gt;">
    <DisplayString>{{{{ size={{tail &lt;= head ? head - tail : buf.cap - tail + head}} }}}}</DisplayString>
    <Expand>
      <Item Name="[size]" ExcludeView="simple">tail &lt;= head ? head - tail : buf.cap - tail + head</Item>
      <Item Name="[capacity]" ExcludeView="simple">buf.cap</Item>
      <CustomListItems>
        <Variable Name="i" InitialValue="tail" />
        <Size>tail &lt;= head ? head - tail : buf.cap - tail + head</Size>
        <Loop>
          <If Condition="i == head">
            <Break/>
          </If>
          <Item>buf.ptr.pointer.__0 + i</Item>
          <Exec>i = (i + 1 == buf.cap ? 0 : i + 1)</Exec>
        </Loop>
      </CustomListItems>
    </Expand>
  </Type>
  <Type Name="alloc::linked_list::LinkedList&lt;*&gt;">
    <DisplayString>{{{{ size={{len}} }}}}</DisplayString>
    <Expand>
      <LinkedListItems>
        <Size>len</Size>
        <HeadPointer>*(alloc::linked_list::Node&lt;$T1&gt; **)&amp;head</HeadPointer>
        <NextPointer>*(alloc::linked_list::Node&lt;$T1&gt; **)&amp;next</NextPointer>
        <ValueNode>element</ValueNode>
      </LinkedListItems>
    </Expand>
  </Type>
  <Type Name="alloc::string::String">
    <DisplayString>{{*(char**)this,[vec.len]}}</DisplayString>
    <StringView>*(char**)this,[vec.len]</StringView>
    <Expand>
      <Item Name="[size]" ExcludeView="simple">vec.len</Item>
      <Item Name="[capacity]" ExcludeView="simple">vec.buf.cap</Item>
      <ArrayItems>
        <Size>vec.len</Size>
        <ValuePointer>*(char**)this</ValuePointer>
      </ArrayItems>
    </Expand>
  </Type>
</AutoVisualizer>
"#));
}

pub fn create_license_mit(mut path: PathBuf) {
    path.push("LICENSE-MIT");
    let year = time::now_utc().tm_year + 1900;
    let mut file = unwrap!(File::create(&path));
    unwrap!(write!(
        &mut file,
        r#"The MIT License (MIT)

Copyright (c) {} Fraser Hutchison

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute,
sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial
portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES
OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
"#,
        year
    ));
}

pub fn create_license_apache(mut path: PathBuf) {
    path.push("LICENSE-APACHE");
    let mut file = unwrap!(File::create(&path));
    unwrap!(write!(
        &mut file,
        r#"                              Apache License
                        Version 2.0, January 2004
                     http://www.apache.org/licenses/

TERMS AND CONDITIONS FOR USE, REPRODUCTION, AND DISTRIBUTION

1. Definitions.

   "License" shall mean the terms and conditions for use, reproduction,
   and distribution as defined by Sections 1 through 9 of this document.

   "Licensor" shall mean the copyright owner or entity authorized by
   the copyright owner that is granting the License.

   "Legal Entity" shall mean the union of the acting entity and all
   other entities that control, are controlled by, or are under common
   control with that entity. For the purposes of this definition,
   "control" means (i) the power, direct or indirect, to cause the
   direction or management of such entity, whether by contract or
   otherwise, or (ii) ownership of fifty percent (50%) or more of the
   outstanding shares, or (iii) beneficial ownership of such entity.

   "You" (or "Your") shall mean an individual or Legal Entity
   exercising permissions granted by this License.

   "Source" form shall mean the preferred form for making modifications,
   including but not limited to software source code, documentation
   source, and configuration files.

   "Object" form shall mean any form resulting from mechanical
   transformation or translation of a Source form, including but
   not limited to compiled object code, generated documentation,
   and conversions to other media types.

   "Work" shall mean the work of authorship, whether in Source or
   Object form, made available under the License, as indicated by a
   copyright notice that is included in or attached to the work
   (an example is provided in the Appendix below).

   "Derivative Works" shall mean any work, whether in Source or Object
   form, that is based on (or derived from) the Work and for which the
   editorial revisions, annotations, elaborations, or other modifications
   represent, as a whole, an original work of authorship. For the purposes
   of this License, Derivative Works shall not include works that remain
   separable from, or merely link (or bind by name) to the interfaces of,
   the Work and Derivative Works thereof.

   "Contribution" shall mean any work of authorship, including
   the original version of the Work and any modifications or additions
   to that Work or Derivative Works thereof, that is intentionally
   submitted to Licensor for inclusion in the Work by the copyright owner
   or by an individual or Legal Entity authorized to submit on behalf of
   the copyright owner. For the purposes of this definition, "submitted"
   means any form of electronic, verbal, or written communication sent
   to the Licensor or its representatives, including but not limited to
   communication on electronic mailing lists, source code control systems,
   and issue tracking systems that are managed by, or on behalf of, the
   Licensor for the purpose of discussing and improving the Work, but
   excluding communication that is conspicuously marked or otherwise
   designated in writing by the copyright owner as "Not a Contribution."

   "Contributor" shall mean Licensor and any individual or Legal Entity
   on behalf of whom a Contribution has been received by Licensor and
   subsequently incorporated within the Work.

2. Grant of Copyright License. Subject to the terms and conditions of
   this License, each Contributor hereby grants to You a perpetual,
   worldwide, non-exclusive, no-charge, royalty-free, irrevocable
   copyright license to reproduce, prepare Derivative Works of,
   publicly display, publicly perform, sublicense, and distribute the
   Work and such Derivative Works in Source or Object form.

3. Grant of Patent License. Subject to the terms and conditions of
   this License, each Contributor hereby grants to You a perpetual,
   worldwide, non-exclusive, no-charge, royalty-free, irrevocable
   (except as stated in this section) patent license to make, have made,
   use, offer to sell, sell, import, and otherwise transfer the Work,
   where such license applies only to those patent claims licensable
   by such Contributor that are necessarily infringed by their
   Contribution(s) alone or by combination of their Contribution(s)
   with the Work to which such Contribution(s) was submitted. If You
   institute patent litigation against any entity (including a
   cross-claim or counterclaim in a lawsuit) alleging that the Work
   or a Contribution incorporated within the Work constitutes direct
   or contributory patent infringement, then any patent licenses
   granted to You under this License for that Work shall terminate
   as of the date such litigation is filed.

4. Redistribution. You may reproduce and distribute copies of the
   Work or Derivative Works thereof in any medium, with or without
   modifications, and in Source or Object form, provided that You
   meet the following conditions:

   (a) You must give any other recipients of the Work or
       Derivative Works a copy of this License; and

   (b) You must cause any modified files to carry prominent notices
       stating that You changed the files; and

   (c) You must retain, in the Source form of any Derivative Works
       that You distribute, all copyright, patent, trademark, and
       attribution notices from the Source form of the Work,
       excluding those notices that do not pertain to any part of
       the Derivative Works; and

   (d) If the Work includes a "NOTICE" text file as part of its
       distribution, then any Derivative Works that You distribute must
       include a readable copy of the attribution notices contained
       within such NOTICE file, excluding those notices that do not
       pertain to any part of the Derivative Works, in at least one
       of the following places: within a NOTICE text file distributed
       as part of the Derivative Works; within the Source form or
       documentation, if provided along with the Derivative Works; or,
       within a display generated by the Derivative Works, if and
       wherever such third-party notices normally appear. The contents
       of the NOTICE file are for informational purposes only and
       do not modify the License. You may add Your own attribution
       notices within Derivative Works that You distribute, alongside
       or as an addendum to the NOTICE text from the Work, provided
       that such additional attribution notices cannot be construed
       as modifying the License.

   You may add Your own copyright statement to Your modifications and
   may provide additional or different license terms and conditions
   for use, reproduction, or distribution of Your modifications, or
   for any such Derivative Works as a whole, provided Your use,
   reproduction, and distribution of the Work otherwise complies with
   the conditions stated in this License.

5. Submission of Contributions. Unless You explicitly state otherwise,
   any Contribution intentionally submitted for inclusion in the Work
   by You to the Licensor shall be under the terms and conditions of
   this License, without any additional terms or conditions.
   Notwithstanding the above, nothing herein shall supersede or modify
   the terms of any separate license agreement you may have executed
   with Licensor regarding such Contributions.

6. Trademarks. This License does not grant permission to use the trade
   names, trademarks, service marks, or product names of the Licensor,
   except as required for reasonable and customary use in describing the
   origin of the Work and reproducing the content of the NOTICE file.

7. Disclaimer of Warranty. Unless required by applicable law or
   agreed to in writing, Licensor provides the Work (and each
   Contributor provides its Contributions) on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
   implied, including, without limitation, any warranties or conditions
   of TITLE, NON-INFRINGEMENT, MERCHANTABILITY, or FITNESS FOR A
   PARTICULAR PURPOSE. You are solely responsible for determining the
   appropriateness of using or redistributing the Work and assume any
   risks associated with Your exercise of permissions under this License.

8. Limitation of Liability. In no event and under no legal theory,
   whether in tort (including negligence), contract, or otherwise,
   unless required by applicable law (such as deliberate and grossly
   negligent acts) or agreed to in writing, shall any Contributor be
   liable to You for damages, including any direct, indirect, special,
   incidental, or consequential damages of any character arising as a
   result of this License or out of the use or inability to use the
   Work (including but not limited to damages for loss of goodwill,
   work stoppage, computer failure or malfunction, or any and all
   other commercial damages or losses), even if such Contributor
   has been advised of the possibility of such damages.

9. Accepting Warranty or Additional Liability. While redistributing
   the Work or Derivative Works thereof, You may choose to offer,
   and charge a fee for, acceptance of support, warranty, indemnity,
   or other liability obligations and/or rights consistent with this
   License. However, in accepting such obligations, You may act only
   on Your own behalf and on Your sole responsibility, not on behalf
   of any other Contributor, and only if You agree to indemnify,
   defend, and hold each Contributor harmless for any liability
   incurred by, or claims asserted against, such Contributor by reason
   of your accepting any such warranty or additional liability.

END OF TERMS AND CONDITIONS

APPENDIX: How to apply the Apache License to your work.

   To apply the Apache License to your work, attach the following
   boilerplate notice, with the fields enclosed by brackets "[]"
   replaced with your own identifying information. (Don't include
   the brackets!)  The text should be enclosed in the appropriate
   comment syntax for the file format. We also recommend that a
   file or class name and description of purpose be included on the
   same "printed page" as the copyright notice for easier
   identification within third-party archives.

Copyright [yyyy] [name of copyright owner]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"#
    ));
}
